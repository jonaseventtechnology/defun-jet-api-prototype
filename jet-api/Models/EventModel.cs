﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace jet_api.Models
{
    public class Event
    {
        public Event() { }
        [Required(AllowEmptyStrings =false), StringLength(5)]
        public string EventRef { get; set; }
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
    public class GetEventRequest
    {
        [Required(AllowEmptyStrings = false), StringLength(5)]
        public string EventRef { get; set; }
    }
    public class GetEventResponse : Event
    {
    }
    public class PostEventRequest : Event
    {
    }
    public class PostEventResponse
    {
    }

    [Serializable]
    internal class EventNotFoundException : Exception
    {
        public EventNotFoundException()
        {
        }

        public EventNotFoundException(string message) : base(message)
        {
        }

        public EventNotFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected EventNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}