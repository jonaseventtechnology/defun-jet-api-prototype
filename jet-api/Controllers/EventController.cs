﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Swashbuckle.Swagger.Annotations;
using System.ComponentModel.DataAnnotations;
using jet_api.Models;
using static jet_api.JET_api;
using System;
using System.Runtime.Serialization;

namespace jet_api.Controllers
{
    /// <summary>
    /// nothing
    /// </summary>
    public class EventController : ApiController
    {
        [Route("Event/{EventRef}")]
        [HttpGet]
        [SwaggerResponse(HttpStatusCode.OK,"", typeof(Event)) ]
        [SwaggerResponse(HttpStatusCode.NotFound) ]
        [SwaggerResponse(HttpStatusCode.InternalServerError)]
        [SwaggerResponse(HttpStatusCode.BadRequest)]
        public HttpResponseMessage Get([FromUri]string EventRef)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    GetEventRequest ger = new GetEventRequest();
                    ger.EventRef = EventRef;
                    Event em = GetEvent(ger);
                    if (em != null)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, em);
                    }
                    else
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.InternalServerError,"Unknown issue");
                    }
                }
                else
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
            }
            catch (EventNotFoundException e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, e.Message);
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }




        [Route("Event")]
        [HttpPost]
        [SwaggerResponse(HttpStatusCode.Created)]
        [SwaggerResponse(HttpStatusCode.InternalServerError)]
        [SwaggerResponse(HttpStatusCode.BadRequest)]
        public HttpResponseMessage Post(Event body)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    PostEventResponse resp = PostEvent(body);
                    if (resp != null)
                    {
                        return Request.CreateResponse(HttpStatusCode.Created);
                    }
                    else
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.InternalServerError,"Internal problem!");
                    }
                }
                else
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest,ModelState);
                }
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }

    }

}
