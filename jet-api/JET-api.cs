﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jet_api.Models;

namespace jet_api
{
    public class JET_api
    {
        public static GetEventResponse GetEvent(GetEventRequest ger)
        {
            GetEventResponse em = new GetEventResponse();
            em.Description = "Test Description";
            em.EventRef = "TEST EventRef";
            em.Name = "TEST Name";
            em.StartDate = DateTime.Now;
            em.EndDate = DateTime.Now.AddDays(2);

            if (ger.EventRef == "FAIL")
            {
                throw new EventNotFoundException("Could not find event with ref: " + ger.EventRef);
            }

            return em;
        }
        public static PostEventResponse PostEvent(Event per)
        {
            return new PostEventResponse();
        }
    }
}