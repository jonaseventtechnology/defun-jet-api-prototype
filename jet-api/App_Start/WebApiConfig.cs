﻿using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Web.Http;

namespace jet_api.App_Start
{
    public class WebApiConfig
    {
        public static void Configure(HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes();

            var mediaType = new MediaTypeHeaderValue("application/json");

            var formatter = new JsonMediaTypeFormatter();
            formatter.SupportedMediaTypes.Clear();
            formatter.SupportedMediaTypes.Add(mediaType);

            config.Formatters.Clear();
            config.Formatters.Add(formatter);
        }
    }
}